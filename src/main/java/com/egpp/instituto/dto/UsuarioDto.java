package com.egpp.instituto.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;


public class UsuarioDto {
	@Getter
	@Setter
	private int id;
	@Getter
	@Setter
	private String username;
	@Getter
	@Setter
	private String email;
	@Getter
	@Setter
	private String password;
	
	
	public String getNombreReal() {
		return "Juanito Perez";
	}
	
	
	public Date getFechaRegistro() {
		return new Date();
	}
	
	
}
