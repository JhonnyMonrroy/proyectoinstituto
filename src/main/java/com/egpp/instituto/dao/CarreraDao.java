package com.egpp.instituto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.egpp.instituto.models.Carrera;

@Repository
@Transactional
public interface CarreraDao extends JpaRepository<Carrera, Integer>{
	
}
