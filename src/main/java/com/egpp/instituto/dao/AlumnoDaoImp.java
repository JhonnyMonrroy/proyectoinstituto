package com.egpp.instituto.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.egpp.instituto.models.Alumno;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;


@Repository
@Transactional
public class AlumnoDaoImp implements AlumnoDao {
	
	@PersistenceContext
    EntityManager entityManager;

	@Transactional
	public List<Alumno> listarAlumnos() {
		
		//String query = "select * from alummnos";
		String query = "FROM Alumno";
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public Alumno obtenerAlumno(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
