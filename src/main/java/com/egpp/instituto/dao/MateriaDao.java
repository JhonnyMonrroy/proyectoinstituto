package com.egpp.instituto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.egpp.instituto.models.Materia;

import jakarta.transaction.Transactional;

@Repository
@Transactional
public interface MateriaDao extends JpaRepository<Materia, Integer>{

}
