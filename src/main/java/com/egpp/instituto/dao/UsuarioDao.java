package com.egpp.instituto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.egpp.instituto.models.Usuario;

import jakarta.transaction.Transactional;

@Repository
@Transactional
public interface UsuarioDao extends JpaRepository<Usuario, Integer>{

	public Usuario findOneByUsername(String username);
	
}
