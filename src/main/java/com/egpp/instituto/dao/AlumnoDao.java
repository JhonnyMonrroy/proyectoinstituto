package com.egpp.instituto.dao;

import java.util.List;

import com.egpp.instituto.models.Alumno;

public interface AlumnoDao {
	
	public List<Alumno> listarAlumnos();
	
	public Alumno obtenerAlumno(int id);
}
