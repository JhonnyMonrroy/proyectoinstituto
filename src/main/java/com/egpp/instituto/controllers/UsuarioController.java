package com.egpp.instituto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.egpp.instituto.dto.UsuarioDto;
import com.egpp.instituto.services.IUsuarioService;

@RestController
@RequestMapping(value = "api/usuarios")
public class UsuarioController {

	@Autowired
	IUsuarioService usuarioService;

	@GetMapping
	public ResponseEntity<List<UsuarioDto>> listarUsuarios(@RequestHeader(value="Authorization") String token) {
		
		if(usuarioService.validarToken(token))
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.listarUsuarios());
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
	}
	
//	@GetMapping
//	public List<UsuarioDto> listarUsuarios() {
//
//		return usuarioService.listarUsuarios();
//	}
	
	@PostMapping
	public ResponseEntity<UsuarioDto> guardarUsuario(@RequestBody UsuarioDto usuarioDto) {

		UsuarioDto resultado = usuarioService.guardar(usuarioDto);
		//return resultado;
		return ResponseEntity.status(HttpStatus.CREATED).body(resultado);
	}
	
	@PutMapping("/{id}")
	public UsuarioDto editarUsuario(@PathVariable int id, @RequestBody UsuarioDto usuarioDto) {
		UsuarioDto usuarioDtoEditado = usuarioService.obtenerUsuario(id);
		usuarioDtoEditado.setEmail(usuarioDto.getEmail());
		
		usuarioDtoEditado = usuarioService.editarUsuario(usuarioDtoEditado);
		
		return usuarioDtoEditado;
	}
	
	@DeleteMapping("/{id}")
	public void eliminarUsuario(@PathVariable int id) {
		
		UsuarioDto usuarioDto = usuarioService.obtenerUsuario(id);
		usuarioService.eliminarUsuario(usuarioDto);
		//System.out.println("Id= "+id);
		return;
	}

}
