package com.egpp.instituto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.egpp.instituto.dto.UsuarioDto;
import com.egpp.instituto.services.IUsuarioService;

@RestController
@RequestMapping(value = "api/auth")
public class AuthController {
	
	@Autowired
	IUsuarioService usuarioService;
	
	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestBody UsuarioDto usuarioDto) {

		String respuesta = usuarioService.autenticarUsuario(usuarioDto);		// true, false
		
		if(!respuesta.equals("FAIL")) {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(respuesta);			//200
		}
			
		else
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("FAIL");		// 401
		
//		if(!respuesta.equals("FAIL"))
//			return ResponseEntity.status(HttpStatus.ACCEPTED).body("OK");			//200
//		else
//			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("FAIL");		// 401
	}
}
