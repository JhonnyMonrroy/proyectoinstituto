package com.egpp.instituto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.egpp.instituto.dao.MateriaDao;
import com.egpp.instituto.models.Materia;

@RestController
public class MateriaController {
	
	@Autowired
	MateriaDao materiaDao;
	
	@RequestMapping(value ="materias")
	public List<Materia> listarMaterias(){
		return materiaDao.findAll();
	}
	
}
