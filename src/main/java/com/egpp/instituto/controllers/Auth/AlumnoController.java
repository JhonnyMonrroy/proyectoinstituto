package com.egpp.instituto.controllers.Auth;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.egpp.instituto.dao.AlumnoDao;
import com.egpp.instituto.models.Alumno;
import com.egpp.instituto.models.Materia;

@RestController
public class AlumnoController {

	@Autowired
	private AlumnoDao alumnoDao;

	@RequestMapping(value = "prueba")
	public String prueba() {
		return "Mi Primer REST";
	}

	/**
	 * Listar a todos los alumnos
	 */
	@RequestMapping(value = "alumnos")
	public List<Alumno> listarAlumnos() {

		List<Alumno> alumnos = alumnoDao.listarAlumnos();

		return alumnos;
	}
	
	/**
	 * Mostrar un alumno
	 */
	@RequestMapping(value = "alumnos/{id}")
	public Alumno obtenerAlumno(@PathVariable int id) {
		
		Alumno alumno = alumnoDao.obtenerAlumno(id);
	
		return alumno;
	}

//	/**
//	 * Mostrar un alumno
//	 */
//	@RequestMapping(value = "alumnos/{id}")
//	public Alumno obtenerAlumno(@PathVariable int id) {
//		Alumno alumno = new Alumno(0, "Juan", "Perez", new Date(), null);
//		alumno.setId(id);
//		return alumno;
//	}

}
