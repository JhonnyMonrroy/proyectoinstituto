package com.egpp.instituto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.egpp.instituto.dao.CarreraDao;
import com.egpp.instituto.models.Carrera;

@RestController
public class CarreraController {
	
	@Autowired
	private CarreraDao carreraDao;
	

	/**
	 * Listar a todos las carrera
	 */
	@RequestMapping(value = "carreras")
	public List<Carrera> listarCarreras() {
		

		List<Carrera> carreras = carreraDao.findAll();

		return carreras;
	}
	
	
}
