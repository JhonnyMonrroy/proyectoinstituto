package com.egpp.instituto.models;

import java.util.Date;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;


@Entity
public class Alumno {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Basic
	private String nombre;
	private String apellido;
	//@Temporal(TemporalType.DATE)
	@Column(name ="fechanac")
	private Date fechaNac;
	
//	@OneToOne
//	private Carrera carrera;
	
	
	public Alumno() {
	}

	public Alumno(int id, String nombre, String apellido, Date fechaNac, Carrera carrera) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaNac = fechaNac;
		//this.carrera = carrera;
	}



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public Date getFechaNac() {
		return fechaNac;
	}


	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	

//	public Carrera getCarrera() {
//		return carrera;
//	}
//
//	public void setCarrera(Carrera carrera) {
//		this.carrera = carrera;
//	}
//	
	
	/**
	 * Metodo que permite mostrar los valores de la clase en forma de cadena
	 * */
	public String toString() {
		return "Alumno [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", fechaNac=" + fechaNac + "]";
	}
}
