package com.egpp.instituto.mapper;

import com.egpp.instituto.dto.UsuarioDto;
import com.egpp.instituto.models.Usuario;

public class UsuarioMapper {
	
	public static UsuarioDto mapper(Usuario usuario){
		UsuarioDto auxDto=new UsuarioDto();
		auxDto.setId(usuario.getId());
		auxDto.setUsername(usuario.getUsername());
		auxDto.setEmail(usuario.getEmail());
		
		return auxDto;
	}
		
}
