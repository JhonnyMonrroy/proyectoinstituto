package com.egpp.instituto.services;

import java.util.List;

import com.egpp.instituto.dto.UsuarioDto;

public interface IUsuarioService {
	
	public List<UsuarioDto> listarUsuarios();

	public UsuarioDto guardar(UsuarioDto usuarioDto);

	public UsuarioDto obtenerUsuario(int id);

	public void eliminarUsuario(UsuarioDto usuarioDto);

	public UsuarioDto editarUsuario(UsuarioDto usuarioDtoEditado);

	public UsuarioDto obtenerUsuarioByUserName(String username);

	public String autenticarUsuario(UsuarioDto usuarioDto);

	public boolean validarToken(String token);
	
}
