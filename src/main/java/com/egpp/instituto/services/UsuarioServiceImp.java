package com.egpp.instituto.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.egpp.instituto.Utils.JWTUtil;
import com.egpp.instituto.dao.UsuarioDao;
import com.egpp.instituto.dto.UsuarioDto;
import com.egpp.instituto.mapper.UsuarioMapper;
import com.egpp.instituto.models.Usuario;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

@Service
public class UsuarioServiceImp implements IUsuarioService {

	@Autowired
	UsuarioDao usuarioDao;

	@Autowired
	JWTUtil jwtUtil;

	@Override
	public List<UsuarioDto> listarUsuarios() {
		List<Usuario> usuariosEntity = usuarioDao.findAll();

		List<UsuarioDto> resultado = new ArrayList<UsuarioDto>();

		for (Usuario us : usuariosEntity)
			resultado.add(UsuarioMapper.mapper(us));
		return resultado;
	}

	@Override
	public UsuarioDto guardar(UsuarioDto usuarioDto) {
		
		Usuario usuario = new Usuario();
		usuario.setEmail(usuarioDto.getEmail());
		usuario.setUsername(usuarioDto.getUsername());
		// deberiamos cifrar la contraseña
		Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2i);
		
		String passwordHasheado = argon2.hash(2, 1024, 2, usuarioDto.getPassword());
		
		usuario.setPassword(passwordHasheado);
		
		usuario = usuarioDao.save(usuario);
		
		return UsuarioMapper.mapper(usuario);
	}

	@Override
	public UsuarioDto obtenerUsuario(int id) {
		Usuario usuario = usuarioDao.findById(id).orElseThrow(); 
		return UsuarioMapper.mapper(usuario);
	}

	@Override
	public void eliminarUsuario(UsuarioDto usuarioDto) {
		Usuario usuario = usuarioDao.findById(usuarioDto.getId()).orElseThrow();
		
		usuarioDao.delete(usuario);
	}

	@Override
	public UsuarioDto editarUsuario(UsuarioDto usuarioDtoEditado) {
		Usuario usuario = usuarioDao.findById(usuarioDtoEditado.getId()).orElseThrow();
		usuario.setEmail(usuarioDtoEditado.getEmail());
		
		usuario = usuarioDao.save(usuario);
		
		return UsuarioMapper.mapper(usuario);
	}

	@Override
	public UsuarioDto obtenerUsuarioByUserName(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String autenticarUsuario(UsuarioDto usuarioDto) {
		Usuario usuario = usuarioDao.findOneByUsername(usuarioDto.getUsername());
		if(usuario!=null){
			
			String passwordHasheado = usuario.getPassword();
			String passwordComun = usuarioDto.getPassword();
			
			System.out.println(passwordHasheado);
			System.out.println(passwordComun);
			
			Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2i);
			
			if(argon2.verify(passwordHasheado, passwordComun)) {
				//Clave = valor
				String jwtToken = jwtUtil.create(String.valueOf(usuario.getId()), usuario.getEmail());
				return jwtToken;
			}
				//return true;
			
//			if(usuario.getPassword().equals(usuarioDto.getPassword()))
//				return true;
		}
			
		return "FAIL";
	}

	@Override
	public boolean validarToken(String token) {
		String idUsuario = jwtUtil.getKey(token);		// obteniendo el id del usuario
		Usuario usuario = usuarioDao.findById(Integer.parseInt(idUsuario)).orElseThrow();
//		if(usuario != null)
//			return true;
//		return false;
		
		return usuario != null;
	}

}
